# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 Nico Epp and Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import my_classes
import pickle
from my_classes import SymbolType, Symbol, RightSidePart, Rule, BNF
from typing import Dict, Set, Tuple, List


def check(bnf: BNF, debug=False) -> bool:
    def print_(*args):
        # function to print only in debug mode
        if debug:
            print('**', *args)

    print_()
    print_('FIRST symbols:')
    for symbol in bnf.rules.keys():
        # these calculated sets are not being used, but we print them for debugging purposes
        fs_set, error_list, error_symbol = get_first_symbols(symbol, [], bnf)
        print_('  {:10s}'.format(symbol), sorted(fs_set))

        for err in error_list:
            print_('WARNING:', err)
        if error_symbol:
            raise ValueError('"error_symbol" should be "None", but is "{}"'.format(error_symbol))

    print_()
    print_('NEXT symbols:')
    next_symbols = get_next_symbols(bnf)
    for symbol in bnf.rules.keys():
        print_('  {:10s}'.format(symbol),
               sorted(next_symbols[symbol]))

    print_()
    print_('M table:')
    m_table = get_m_table(bnf, next_symbols)
    terminals = bnf.get_terminals()
    terminals.append(my_classes.END_OF_INPUT)

    for nt in bnf.rules.keys():
        print_(' ', '-' * 40)
        for t in terminals:
            cell = m_table.get((nt, t), [])

            content = ''
            if len(cell) == 1:
                content = cell[0]
            elif len(cell) > 1:
                content = 'ERROR: [{}]'.format(
                    ', '.join(
                        '"{}"'.format(e) for e in cell))

            print_('   {:10s} {:10s} {}'.format(nt, t, content))

    return bool(['ambiguous rule'
                 for e in m_table.values()
                 if len(e) > 1])


def get_first_symbols(symbol: Symbol,
                      breadcrumbs: List[Symbol],
                      bnf: BNF) -> Tuple[Set[Symbol], List[str], Symbol]:
    if symbol not in bnf.rules:
        raise ValueError('grammar with errors: non-terminal "{}" has no rules'.format(symbol))

    breadcrumbs.append(symbol)

    first_symbols_set = set()
    error_list = []
    error_symbol = None

    # get 'FIRST symbols' for each part and join them
    for part in bnf.rules[symbol].right_side:
        _first_symbols_set, _error_list, _error_symbol = _get_first_symbols_of_part(
            part, list(breadcrumbs), bnf)

        if _error_symbol and not error_symbol:
            # do not override an existing error_symbol
            error_symbol = _error_symbol

        first_symbols_set.update(_first_symbols_set)
        error_list.extend(_error_list)

    return first_symbols_set, error_list, error_symbol


def _get_first_symbols_of_part(part: RightSidePart,
                               breadcrumbs: List[Symbol],
                               bnf: BNF) -> Tuple[Set[Symbol], List[str], Symbol]:
    first_symbols_set = set()
    error_list = []
    error_symbol = None

    if part[0].type == SymbolType.empty:
        # this part contains EMPTY
        first_symbols_set.add(part[0])
        return first_symbols_set, error_list, error_symbol

    for symbol in part:
        if symbol.type == SymbolType.terminal:
            first_symbols_set.add(symbol)
            return first_symbols_set, error_list, error_symbol

        if _check_indirect_recursion(breadcrumbs + [symbol]):
            # simply return with an error message
            error_symbol = symbol
            continue

        # get 'FIRST symbols' for this non-terminal
        _first_symbols_set, _error_list, _error_symbol = get_first_symbols(
            symbol, list(breadcrumbs), bnf)

        error_list.extend(_error_list)

        if _error_symbol:
            if _error_symbol == symbol:
                # solve the indirect recursion here and don't send it further up the call tree
                error_symbol = None

                if my_classes.EMPTY not in _first_symbols_set:
                    # EMPTY element is not present; just return
                    error_list.append(
                        'possible indirect recursion: [{}]'.format(
                            my_classes.SYMBOL_SEP.join(breadcrumbs)))
                    return first_symbols_set, error_list, error_symbol

            elif not error_symbol:
                # do not override an existing error_symbol
                error_symbol = _error_symbol
        else:
            if my_classes.EMPTY in _first_symbols_set:
                # add symbols to set (excluding EMPTY) and continue searching
                _first_symbols_set.remove(my_classes.EMPTY)
                first_symbols_set.update(_first_symbols_set)
            else:
                # EMPTY element is not present; add symbols and return
                first_symbols_set.update(_first_symbols_set)
                return first_symbols_set, error_list, error_symbol

    # all non-terminals have EMPTY in their 'FIRST symbols'
    first_symbols_set.add(Symbol(my_classes.EMPTY))
    return first_symbols_set, error_list, error_symbol


def _check_indirect_recursion(breadcrumbs: List[Symbol]) -> bool:
    symbol = breadcrumbs[-1]

    # Only detect recursion patterns where the last symbol appears 3 or more times
    if len(list(filter(lambda s: s == symbol, breadcrumbs))) < 3:
        return False

    # Detect for example the breadcrumbs pattern: A A A
    if breadcrumbs[-1] == breadcrumbs[-2] == breadcrumbs[-3]:
        return True

    # split breadcrumbs on last symbol
    part_list = []      # type: List[List[Symbol]]
    part = []
    for e in breadcrumbs:
        if e == symbol:
            if part:
                part_list.append(part)
            part = []
        else:
            part.append(e)

    # look for second repetition of recursion pattern
    # e.g. in breadcrumbs: A B C D A B C D A
    # where symbol is A, the part_list would be [[B, C, D], [B, C, D]
    if len(part_list) > 1:
        if part_list[-1] == part_list[-2]:
            rev_pos = -1 * (len(part_list[-1]) * 2 + 3)
            if len(breadcrumbs) >= rev_pos * -1:
                # check the symbols of the middle and the last A
                if breadcrumbs[rev_pos] == symbol:
                    return True
    return False


def get_next_symbols(bnf: BNF) -> Dict[Symbol, Set[Symbol]]:
    next_symbols = {
        s: {
            'next': set(),
            'linked_sets': set(),
        }
        for s in bnf.rules.keys()}      # type: Dict[Symbol, Dict[str, Set]]

    # add END_OF_INPUT to start symbol, which should be the first symbol in bnf.rules
    for symbol in bnf.rules.keys():
        next_symbols[symbol]['next'].add(my_classes.END_OF_INPUT)
        break

    while True:
        old_d = pickle.dumps(next_symbols)

        for rule in bnf.rules.values():
            for part in rule.right_side:
                # search for parts with form "A -> alpha B beta"
                for i in range(len(part) - 1):
                    if part[i].type == SymbolType.non_terminal:
                        fs_of_beta, error_list, error_symbol = _get_first_symbols_of_part(
                            part[i+1:], [], bnf)

                        has_empty = False
                        if my_classes.EMPTY in fs_of_beta:
                            # "A -> alpha B beta" when beta contains EMPTY
                            has_empty = True
                            fs_of_beta.remove(my_classes.EMPTY)

                        next_symbols[part[i]]['next'].update(fs_of_beta)

                        if has_empty:
                            # all symbols from "next(A)" will be in "next(B)"
                            next_symbols[part[i]]['linked_sets'].add(rule.left_side)

                # search for parts with form "A -> alpha B"
                if part[-1].type == SymbolType.non_terminal:
                    # all symbols from "next(A)" will be in "next(B)"
                    next_symbols[part[-1]]['linked_sets'].add(rule.left_side)

        # copy symbols between linked sets
        for d in next_symbols.values():
            for symbol in d['linked_sets']:
                d['next'].update(next_symbols[symbol]['next'])

        # check for changes
        if old_d == pickle.dumps(next_symbols):
            break

    return {s: d['next'] for s, d in next_symbols.items()}


def get_m_table(bnf: BNF, next_symbols: Dict[Symbol, Set[Symbol]]) -> Dict[Tuple, List[Rule]]:
    m_table = {}    # type: Dict[Tuple[Symbol, Symbol], List[Rule]]

    for rule in bnf.rules.values():
        for part in rule.right_side:
            fs_set, error_list, error_symbol = _get_first_symbols_of_part(part, [], bnf)

            if my_classes.EMPTY in fs_set:
                fs_set.remove(my_classes.EMPTY)

                # add rule "A -> _" for each terminal in NEXT
                for t in next_symbols[rule.left_side]:
                    k = (rule.left_side, t)
                    m_table.setdefault(k, [])
                    m_table[k].append(
                        Rule(rule.left_side, RightSidePart([Symbol(my_classes.EMPTY), ])))

            # add rule "A -> part" for each terminal in FIRST
            for t in fs_set:
                k = (rule.left_side, t)
                m_table.setdefault(k, [])
                m_table[k].append(
                    Rule(rule.left_side, part))

    return m_table
