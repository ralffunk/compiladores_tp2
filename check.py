# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 Nico Epp and Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import ambiguity
import common_factors
import my_classes
import recursion
from my_classes import Symbol, RightSidePart, Rule, BNF


def parse_line(line: str) -> Rule:
    if my_classes.CONNECTOR not in line:
        raise ValueError('no connector symbol found in line')

    left, right = line.split(my_classes.CONNECTOR, maxsplit=1)

    # create rule with left side
    rule = Rule(Symbol(left))

    # iterate over parts from the right side
    for part in right.split('|'):
        # a ValueError might be thrown here
        elem = RightSidePart([Symbol(s)
                              for s in part.split(my_classes.SYMBOL_SEP)
                              if s])

        # add to set of right side parts of this rule; duplicates will be ignored
        rule.add_right_side(elem)

    return rule


def read_rules() -> BNF:
    print()
    print('Insert BNF rules using the following format:')
    print('  terminals        =>', 'id of lower case letters and digits')
    print('  non-terminals    =>', 'id of upper case letters and digits')
    print('  other symbols    =>', '+ - * / ( )')
    print('  empty            =>', my_classes.EMPTY)
    print('  connector        =>', my_classes.CONNECTOR)
    print('  symbol separator =>', 'whitespace')
    print('  end of BNF       =>', 'empty line')
    print()

    bnf = BNF()

    while True:
        line = input('>> ')
        if not line:
            break  # empty line: end of BNF

        try:
            rule = parse_line(line)

            # if the left side of the rule already exists, the new right side parts will be added
            # to the existing ones, but avoiding duplicates
            bnf.add_rule(rule)

            print('   OK:', rule)
        except ValueError as err:
            print('   ERROR:', err)

    return bnf


def print_bnf(bnf: BNF, comment: str):
    print()
    print('BNF {}'.format(comment))
    for r in bnf.rules.values():
        for s in r.as_multiple_strings():
            print(' ', s)


if __name__ == '__main__':
    STARS = '*' * 70
    print()
    print(STARS)
    print(STARS)
    print('BNF grammar validator')
    print(STARS)
    print(STARS)

    bnf_1 = read_rules()
    print_bnf(bnf_1, '1')

    print()
    print(STARS)
    print('removing common factors ...')
    bnf_2 = common_factors.remove(bnf_1)
    print_bnf(bnf_2, '2')

    print()
    print(STARS)
    print('removing recursion ...')
    bnf_3 = recursion.remove(bnf_2)
    print_bnf(bnf_3, '3')

    print()
    print(STARS)
    print('checking ambiguity ...')
    is_ambiguous = ambiguity.check(bnf_3, debug=True)

    print()
    print(STARS)
    if is_ambiguous:
        print('ERROR: this grammar is ambiguous')
    else:
        print('OK: this grammar is NOT ambiguous')
