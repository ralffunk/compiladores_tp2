# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 Nico Epp and Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import my_classes
from my_classes import Symbol, RightSidePart, Rule, BNF, Node
from typing import List


def remove(old_bnf: BNF, debug=False) -> BNF:
    def print_(*args):
        # function to print only in debug mode
        if debug:
            print('**', *args)

    new_bnf = BNF()

    for rule in old_bnf.rules.values():
        # make a tree of nodes
        node = _make_node(rule.right_side)

        # print for debugging
        print_()
        print_('tree for "{}"'.format(rule.left_side))
        for s in node.to_strings(0):
            print_(s)

        # make new rules from tree
        for r in _make_rules_from_node(rule.left_side, 1, node):
            new_bnf.add_rule(r)

    return new_bnf


def _make_node(part_list: List[RightSidePart]) -> Node:
    if len(part_list) == 1:
        # only one part is present, no factoring needed
        return Node(list(part_list[0]))

    # create node with common prefix, which can be empty
    node = Node(_get_common_prefix(part_list))
    prefix_len = len(node.prefix)

    # process parts based on the common prefix
    new_part_list = []
    for part in part_list:
        if prefix_len:
            if len(part) > prefix_len:
                # there are more symbols after the common prefix; add that tail to the list
                new_part_list.append(RightSidePart(part[prefix_len:]))
            else:
                # no symbols after common prefix; add EMPTY
                new_part_list.append(RightSidePart([Symbol(my_classes.EMPTY), ]))
        else:
            # no common prefix; place all parts in the new list without modifications
            new_part_list.append(part)

    # make node children
    first_symbols = set(part[0] for part in new_part_list)
    for symbol in first_symbols:
        # get all parts that start with this symbol and make a new node from them
        node.add_child(
            _make_node([part
                        for part in new_part_list
                        if symbol == part[0]]))

    return node


def _get_common_prefix(part_list: List[RightSidePart]) -> List[Symbol]:
    prefix = []

    if part_list:
        for symbols in zip(*part_list):
            if len(set(symbols)) == 1:
                # if all parts have the same symbol, append it to the common prefix
                prefix.append(symbols[0])
            else:
                # different symbols
                break

    return prefix


def _make_rules_from_node(left_side: Symbol, i: int, node: Node) -> List[Rule]:
    if not (0 < i < 100):
        # we create new non-terminals by appending 2 digits to the old ones, so values with more
        # than 2 digits can create errors
        raise ValueError('expected "0 < i < 100", but found "i = {}"'.format(i))

    rule_list = []

    if node.children:
        if node.prefix:
            # make rule that has a new non-terminal at the end of the common prefix
            new_left_side = Symbol(left_side + '{:02d}'.format(i))
            new_right_side = node.prefix + (new_left_side, )
            rule_list.append(
                Rule(left_side, RightSidePart(new_right_side)))
        else:
            # no common prefix, so there is no new non-terminal necessary
            new_left_side = left_side

        # add rules for each child node
        for j, ch in enumerate(node.children):
            rule_list.extend(
                _make_rules_from_node(new_left_side, j+1, ch))
    else:
        # node has no children; make a single rule
        rule_list.append(
            Rule(left_side, RightSidePart(node.prefix)))

    return rule_list
