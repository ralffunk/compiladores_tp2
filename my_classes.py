# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 Nico Epp and Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import collections
import re
from enum import Enum
from typing import Dict, List, Iterable


CONNECTOR = '->'
SYMBOL_SEP = ' '
EMPTY = '_'
END_OF_INPUT = '$'


class SymbolType(Enum):
    non_terminal = 1
    terminal = 2
    empty = 3


class Symbol(str):

    def __new__(cls, content: str):
        striped_content = content.strip()

        if striped_content == EMPTY:
            s = super().__new__(cls, striped_content)
            s.type = SymbolType.empty
            return s

        if re.match(r'^[+\-*/()]$', striped_content):
            s = super().__new__(cls, striped_content)
            s.type = SymbolType.terminal
            return s

        if re.match(r'^[a-z][a-z0-9]*$', striped_content):
            # is all lower case letters or digits, starting with a letter
            s = super().__new__(cls, striped_content)
            s.type = SymbolType.terminal
            return s

        if re.match(r'^[A-Z][A-Za-z0-9]*$', striped_content):
            # has at least one char and starts with upper case letter
            s = super().__new__(cls, striped_content.upper())
            s.type = SymbolType.non_terminal
            return s

        raise ValueError('"{}" is not a valid symbol'.format(content))


class RightSidePart(tuple):

    def __new__(cls, content: List[Symbol]):
        if not content:
            # there is not even an EMPTY element in this part
            raise ValueError('right side part cannot be empty')

        if len(list(content)) > 1 and list(filter(lambda x: x.type == SymbolType.empty, content)):
            # there is an EMPTY element together with other symbols
            raise ValueError('EMPTY can not be mixed with other symbols')

        return super().__new__(cls, content)


class Rule:

    def __init__(self, left_side: Symbol, right_side_part: RightSidePart=None):
        if left_side.type != SymbolType.non_terminal:
            raise ValueError('Left side has to be a NON_TERMINAL')

        # left side cannot be changed
        self._left_side = left_side

        # right side can be extended, but its elements are unique and non-mutable
        self._right_side = set()
        if right_side_part:
            self._right_side.add(right_side_part)

    @property
    def left_side(self) -> Symbol:
        return self._left_side

    @property
    def right_side(self) -> List[RightSidePart]:
        return sorted(
            self._right_side,
            key=lambda x: (x[0].type.value, x))     # little trick to move EMPTY to the end

    def add_right_side(self, new_part: RightSidePart):
        # this part will only be added if it does not already exist in the set
        self._right_side.add(new_part)

    def __str__(self):
        return '{} {} {}'.format(
            str(self._left_side),
            CONNECTOR,
            ' | '.join(SYMBOL_SEP.join(part) for part in self.right_side))

    def as_multiple_strings(self) -> List[str]:
        rules = []
        for part in self.right_side:
            rules.append('{} {} {}'.format(
                str(self._left_side),
                CONNECTOR,
                SYMBOL_SEP.join(part)))

        return rules

    @property
    def has_recursion(self) -> bool:
        # check if this rule has left side recursion
        return bool([part
                     for part in self.right_side
                     if self.left_side == part[0]])


class BNF:

    def __init__(self):
        # the dict keys will be the left sides, the values will be Rule instances
        self.rules = collections.OrderedDict()      # type: Dict[Symbol, Rule]

    def add_rule(self, rule: Rule):
        if rule.left_side in self.rules:
            # left side already exists, add right parts to existing rule instance
            for part in rule.right_side:
                self.rules[rule.left_side].add_right_side(part)
        else:
            # add this rule
            self.rules[rule.left_side] = rule

    def get_terminals(self) -> List[Symbol]:
        s_set = set()

        for rule in self.rules.values():
            for part in rule.right_side:
                for symbol in part:
                    if symbol.type == SymbolType.terminal:
                        s_set.add(symbol)

        return sorted(s_set)


class Node(tuple):

    def __new__(cls, prefix: Iterable[Symbol]):
        n = super().__new__(cls, prefix)
        n._children = []                    # type: List[Node]
        return n

    @property
    def prefix(self):
        return self

    @property
    def children(self) -> 'List[Node]':
        return sorted(self._children, key=lambda x: (x[0].type.value, x))

    def add_child(self, n: 'Node'):
        self._children.append(n)

    def to_strings(self, level: int) -> List[str]:
        str_list = ['{}{}[{}]'.format(' | ' * (level-1), ' |-', SYMBOL_SEP.join(self)), ]

        for ch in self.children:
            str_list.extend(ch.to_strings(level + 1))

        return str_list
