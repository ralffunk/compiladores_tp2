# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 Nico Epp and Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import my_classes
from my_classes import SymbolType, Symbol, RightSidePart, Rule, BNF
from typing import List


def remove(old_bnf: BNF, debug=False) -> BNF:
    def print_(*args):
        # function to print only in debug mode
        if debug:
            print('**', *args)

    new_bnf = BNF()

    print_()
    print_('rules with recursion')
    for rule in old_bnf.rules.values():
        if rule.has_recursion:
            # transform this rule and add the resulting new rules to the output
            print_('  original:', rule)
            for r in _transform(rule):
                print_('  new:     ', r)
                new_bnf.add_rule(r)
            print_()
        else:
            # add unmodified rule to output
            new_bnf.add_rule(rule)

    return new_bnf


def _transform(rule: Rule) -> List[Rule]:
    # original: A -> A alpha | beta_1 | beta_2
    # new:      A -> beta_1 R | beta_2 R
    # new:      R -> alpha R | _

    r_symbol = Symbol(rule.left_side + 'R')

    # extract alpha and beta
    alpha = []
    beta_list = []
    for part in rule.right_side:
        if rule.left_side == part[0]:
            # alpha will be a slice of 'part' without the first element
            alpha = list(part[1:])
        else:
            beta_list.append(part)

    if not beta_list:
        # there is always at least one beta part
        beta_list.append(RightSidePart([Symbol(my_classes.EMPTY), ]))

    # new_rule_1: A -> beta_1 R | beta_2 R
    new_rule_1 = Rule(rule.left_side)
    for part in beta_list:
        if part[0].type == SymbolType.empty:
            # if its the EMPTY element, add only 'r_symbol'
            new_rule_1.add_right_side(RightSidePart([r_symbol, ]))
        else:
            # add 'beta' followed by 'r_symbol'
            new_rule_1.add_right_side(RightSidePart(list(part) + [r_symbol, ]))

    # new_rule_2: R -> alpha R | _
    new_rule_2 = Rule(r_symbol)
    new_rule_2.add_right_side(RightSidePart(alpha + [r_symbol, ]))
    new_rule_2.add_right_side(RightSidePart([Symbol('_'), ]))

    return [new_rule_1, new_rule_2]
