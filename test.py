# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 Nico Epp and Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import ambiguity
import common_factors
import check
import recursion
import unittest
from my_classes import SYMBOL_SEP, SymbolType, Symbol, RightSidePart, Rule, BNF


class MyTest(unittest.TestCase):

    def test_valid_symbols(self):
        symbols = (
            (SymbolType.empty, '_', '_'),
            (SymbolType.empty, '_  ', '_'),
            (SymbolType.empty, ' _', '_'),
            (SymbolType.terminal, 'a', 'a'),
            (SymbolType.terminal, 'a2 ', 'a2'),
            (SymbolType.terminal, 'abcde', 'abcde'),
            (SymbolType.terminal, 'cde4 ', 'cde4'),
            (SymbolType.terminal, ' de ', 'de'),
            (SymbolType.terminal, '+  ', '+'),
            (SymbolType.terminal, '- ', '-'),
            (SymbolType.terminal, '* ', '*'),
            (SymbolType.terminal, ' /', '/'),
            (SymbolType.terminal, '( ', '('),
            (SymbolType.terminal, ' )', ')'),
            (SymbolType.terminal, ' ) ', ')'),
            (SymbolType.non_terminal, 'A', 'A'),
            (SymbolType.non_terminal, 'Aa', 'AA'),
            (SymbolType.non_terminal, 'Aa435Fgh', 'AA435FGH'),
            (SymbolType.non_terminal, ' Bc', 'BC'),
        )
        for i, (s_type, original_str, final_str) in enumerate(symbols):
            with self.subTest(i=i):
                symbol = Symbol(original_str)
                self.assertEqual(symbol.type, s_type)
                self.assertEqual(symbol, final_str)

    def test_invalid_symbols(self):
        symbols = (
            '',
            ' ',
            '_+',
            '++',
            '+ -',
            '.',
            '-a',
            '123',
            '1g',
            'ab a',
            'aAa* ',
            '/*( ',
            '/alfa ',
            '_ _',
            'ña',
            'ó_W 1',
        )
        for i, s in enumerate(symbols):
            with self.subTest(i=i):
                with self.assertRaises(ValueError):
                    Symbol(s)

    def test_valid_right_side_part(self):
        parts = (
            ('_',
             [SymbolType.empty, ]),
            ('alfa1 + Abc',
             [SymbolType.terminal, SymbolType.terminal, SymbolType.non_terminal]),
            ('a + id / Avc',
             [SymbolType.terminal, SymbolType.terminal, SymbolType.terminal, SymbolType.terminal,
              SymbolType.non_terminal]),
        )
        for i, (part, final_iterable) in enumerate(parts):
            with self.subTest(i=i):
                elem = RightSidePart([Symbol(s)
                                      for s in part.split(SYMBOL_SEP)
                                      if s])
                self.assertEqual([e.type for e in elem], final_iterable)

    def test_invalid_right_side_part(self):
        parts = (
            '',
            ' ',
            '_ c',
            ' + aasd A _',
        )
        for i, part in enumerate(parts):
            with self.subTest(i=i):
                with self.assertRaises(ValueError):
                    RightSidePart([Symbol(s)
                                   for s in part.split(SYMBOL_SEP)
                                   if s])

    def test_valid_rules(self):
        left_side_symbols = (
            ('A', 'A'),
            ('C2c', 'C2C'),
            ('BetA', 'BETA'),
        )
        for i, (original_str, final_str) in enumerate(left_side_symbols):
            with self.subTest(i=i):
                self.assertEqual(Rule(Symbol(original_str)).left_side, final_str)

    def test_invalid_rules(self):
        left_side_symbols = (
            '',
            'a',
            '1a',
            '_',
            '+',
        )
        for i, s in enumerate(left_side_symbols):
            with self.subTest(i=i):
                with self.assertRaises(ValueError):
                    Rule(Symbol(s))

    def test_line_parsing(self):
        rules = (
            ('A -> Bb + id | f',
             2,
             [3, 1]),
            ('A1 -> Bb + id | f / | _',
             3,
             [3, 2, 1]),
            ('BETA->_ | ( Bb - id ) | fe a',
             3,
             [5, 2, 1]),
        )
        for i, (line, num_parts, parts_len) in enumerate(rules):
            with self.subTest(i=i):
                rule = check.parse_line(line)
                self.assertEqual(len(rule.right_side), num_parts)
                self.assertEqual([len(e) for e in rule.right_side], parts_len)

    def test_bnf_processing(self):
        data = (
            (       # i = 0
                (
                    'A -> aaa b c | aaa b c d e f | aaa b c d e g | aaa b c x y z + | x y | _',
                ),
                (
                    'A -> aaa b c A01 | x y | _',
                    'A01 -> d e A0101 | x y z + | _',
                    'A0101 -> f | g',
                ),
                (
                    'A -> aaa b c A01 | x y | _',
                    'A01 -> d e A0101 | x y z + | _',
                    'A0101 -> f | g',
                ),
                {
                    'A': [{'aaa', 'x', '_'}, 0],
                    'A01': [{'d', 'x', '_'}, 0],
                    'A0101': [{'f', 'g'}, 0],
                },
                {
                    'A': {'$'},
                    'A01': {'$'},
                    'A0101': {'$'},
                },
                {},
                False,
            ),
            (       # i = 1
                (
                    'B -> b b c | b b c d ',
                ),
                (
                    'B -> b b c B01',
                    'B01 -> d | _',
                ),
                (
                    'B -> b b c B01',
                    'B01 -> d | _',
                ),
                {
                    'B': [{'b'}, 0],
                    'B01': [{'d', '_'}, 0],
                },
                {
                    'B': {'$'},
                    'B01': {'$'},
                },
                {},
                False,
            ),
            (       # i = 2
                (
                    'C -> x ALL | x | C d e | _',
                    'ALL -> ( C ) | id',
                ),
                (
                    'C -> C d e | x C02 | _',
                    'C02 -> ALL | _',
                    'ALL -> ( C ) | id',
                ),
                (
                    'C -> CR | x C02 CR',
                    'CR -> d e CR | _',
                    'C02 -> ALL | _',
                    'ALL -> ( C ) | id',
                ),
                {
                    'C': [{'d', 'x', '_'}, 0],
                    'CR': [{'d', '_'}, 0],
                    'C02': [{'(', 'id', '_'}, 0],
                    'ALL': [{'(', 'id'}, 0],
                },
                {
                    'C': {'$', ')'},
                    'CR': {'$', ')'},
                    'C02': {'$', ')', 'd'},
                    'ALL': {'$', ')', 'd'},
                },
                {},
                False,
            ),
            (       # i = 3
                (
                    'E -> T EE',
                    'EE -> + T EE | _',
                    'T -> F TT',
                    'TT -> * F TT | _',
                    'F -> ( E ) | id',
                ),
                (
                    'E -> T EE',
                    'EE -> + T EE | _',
                    'T -> F TT',
                    'TT -> * F TT | _',
                    'F -> ( E ) | id',
                ),
                (
                    'E -> T EE',
                    'EE -> + T EE | _',
                    'T -> F TT',
                    'TT -> * F TT | _',
                    'F -> ( E ) | id',
                ),
                {
                    'E': [{'(', 'id'}, 0],
                    'EE': [{'+', '_'}, 0],
                    'T': [{'(', 'id'}, 0],
                    'TT': [{'*', '_'}, 0],
                    'F': [{'(', 'id'}, 0],
                },
                {
                    'E': {'$', ')'},
                    'EE': {'$', ')'},
                    'T': {'$', ')', '+'},
                    'TT': {'$', ')', '+'},
                    'F': {'$', ')', '+', '*'},
                },
                {
                    ('E', '('): ['E -> T EE'],
                    ('E', 'id'): ['E -> T EE'],
                    ('EE', ')'): ['EE -> _'],
                    ('EE', '+'): ['EE -> + T EE'],
                    ('EE', '$'): ['EE -> _'],
                    ('T', '('): ['T -> F TT'],
                    ('T', 'id'): ['T -> F TT'],
                    ('TT', ')'): ['TT -> _'],
                    ('TT', '*'): ['TT -> * F TT'],
                    ('TT', '+'): ['TT -> _'],
                    ('TT', '$'): ['TT -> _'],
                    ('F', '('): ['F -> ( E )'],
                    ('F', 'id'): ['F -> id'],
                },
                False,
            ),
            (       # i = 4
                (
                    'P -> if E then P PP | a',
                    'PP -> else P | _',
                    'E -> b',
                ),
                (
                    'P -> a | if E then P PP',
                    'PP -> else P | _',
                    'E -> b',
                ),
                (
                    'P -> a | if E then P PP',
                    'PP -> else P | _',
                    'E -> b',
                ),
                {
                    'P': [{'if', 'a'}, 0],
                    'PP': [{'_', 'else'}, 0],
                    'E': [{'b', }, 0],
                },
                {
                    'P': {'$', 'else'},
                    'PP': {'$', 'else'},
                    'E': {'then', },
                },
                {
                    ('P', 'if'): ['P -> if E then P PP'],
                    ('P', 'a'): ['P -> a'],
                    ('PP', 'else'): ['PP -> else P', 'PP -> _'],    # this is an case of ambiguity
                    ('PP', '$'): ['PP -> _'],
                    ('E', 'b'): ['E -> b'],
                },
                True,
            ),
            (       # i = 5
                (
                    'E -> E E b | x',
                ),
                (
                    'E -> E E b | x',
                ),
                (
                    'E -> x ER',
                    'ER -> E b ER | _',
                ),
                {
                    'E': [{'x', }, 0],
                    'ER': [{'x', '_'}, 0],
                },
                {
                    'E': {'$', 'b'},
                    'ER': {'$', 'b'},
                },
                {
                    ('E', 'x'): ['E -> x ER'],
                    ('ER', 'x'): ['ER -> E b ER'],
                    ('ER', 'b'): ['ER -> _'],
                    ('ER', '$'): ['ER -> _'],
                },
                False,
            ),
            (       # i = 6
                (
                    'E -> E E b',
                ),
                (
                    'E -> E E b',
                ),
                (
                    'E -> ER',
                    'ER -> E b ER | _',
                ),
                {
                    'E': [{'b', '_'}, 0],
                    'ER': [{'b', '_'}, 0],
                },
                {
                    'E': {'$', 'b'},
                    'ER': {'$', 'b'},
                },
                {
                    ('E', 'b'): ['E -> _', 'E -> ER'],
                    ('E', '$'): ['E -> _'],
                    ('ER', 'b'): ['ER -> E b ER', 'ER -> _'],
                    ('ER', '$'): ['ER -> _'],
                },
                True,
            ),
            (       # i = 7
                (
                    'A -> B b | a',
                    'B -> A g | h',
                ),
                (
                    'A -> B b | a',
                    'B -> A g | h',
                ),
                (
                    'A -> B b | a',
                    'B -> A g | h',
                ),
                {
                    'A': [{'a', 'h'}, 1],
                    'B': [{'a', 'h'}, 1],
                },
                {
                    'A': {'$', 'g'},
                    'B': {'b'},
                },
                {
                    ('A', 'a'): ['A -> B b', 'A -> a'],
                    ('A', 'h'): ['A -> B b'],
                    ('B', 'a'): ['B -> A g'],
                    ('B', 'h'): ['B -> A g', 'B -> h'],
                },
                True,
            ),
            (       # i = 8
                (
                    'A -> B b | a | _',
                    'B -> A g | h',
                ),
                (
                    'A -> B b | a | _',
                    'B -> A g | h',
                ),
                (
                    'A -> B b | a | _',
                    'B -> A g | h',
                ),
                {
                    'A': [{'a', 'g', 'h', '_'}, 0],
                    'B': [{'a', 'g', 'h'}, 1],
                },
                {
                    'A': {'$', 'g'},
                    'B': {'b'},
                },
                {
                    ('A', 'a'): ['A -> B b', 'A -> a'],
                    ('A', 'g'): ['A -> B b', 'A -> _'],
                    ('A', 'h'): ['A -> B b'],
                    ('A', '$'): ['A -> _'],
                    ('B', 'a'): ['B -> A g'],
                    ('B', 'g'): ['B -> A g'],
                    ('B', 'h'): ['B -> A g', 'B -> h'],
                },
                True,
            ),
            (       # i = 9
                (
                    'A -> B b | a',
                    'B -> A g | h | _',
                ),
                (
                    'A -> B b | a',
                    'B -> A g | h | _',
                ),
                (
                    'A -> B b | a',
                    'B -> A g | h | _',
                ),
                {
                    'A': [{'a', 'b', 'h'}, 1],
                    'B': [{'a', 'b', 'h', '_'}, 0],
                },
                {
                    'A': {'$', 'g'},
                    'B': {'b'},
                },
                {
                    ('A', 'a'): ['A -> B b', 'A -> a'],
                    ('A', 'b'): ['A -> B b'],
                    ('A', 'h'): ['A -> B b'],
                    ('B', 'a'): ['B -> A g'],
                    ('B', 'b'): ['B -> A g', 'B -> _'],
                    ('B', 'h'): ['B -> A g', 'B -> h'],
                },
                True,
            ),
            (       # i = 10
                (
                    'A -> B b | a | _',
                    'B -> A g | h | _',
                ),
                (
                    'A -> B b | a | _',
                    'B -> A g | h | _',
                ),
                (
                    'A -> B b | a | _',
                    'B -> A g | h | _',
                ),
                {
                    'A': [{'a', 'b', 'g', 'h', '_'}, 0],
                    'B': [{'a', 'b', 'g', 'h', '_'}, 0],
                },
                {
                    'A': {'$', 'g'},
                    'B': {'b'},
                },
                {
                    ('A', 'a'): ['A -> B b', 'A -> a'],
                    ('A', 'b'): ['A -> B b'],
                    ('A', 'g'): ['A -> B b', 'A -> _'],
                    ('A', 'h'): ['A -> B b'],
                    ('A', '$'): ['A -> _'],
                    ('B', 'a'): ['B -> A g'],
                    ('B', 'b'): ['B -> A g', 'B -> _'],
                    ('B', 'g'): ['B -> A g'],
                    ('B', 'h'): ['B -> A g', 'B -> h'],
                },
                True,
            ),
            (       # i = 11
                (
                    'A -> B b | a | _',
                    'B -> C c | b | _',
                    'C -> D d | c | _',
                    'D -> A g | h | _',
                ),
                (
                    'A -> B b | a | _',
                    'B -> C c | b | _',
                    'C -> D d | c | _',
                    'D -> A g | h | _',
                ),
                (
                    'A -> B b | a | _',
                    'B -> C c | b | _',
                    'C -> D d | c | _',
                    'D -> A g | h | _',
                ),
                {
                    'A': [{'a', 'b', 'c', 'd', 'g', 'h', '_'}, 0],
                    'B': [{'a', 'b', 'c', 'd', 'g', 'h', '_'}, 0],
                    'C': [{'a', 'b', 'c', 'd', 'g', 'h', '_'}, 0],
                    'D': [{'a', 'b', 'c', 'd', 'g', 'h', '_'}, 0],
                },
                {
                    'A': {'$', 'g'},
                    'B': {'b'},
                    'C': {'c'},
                    'D': {'d'},
                },
                {
                    ('A', 'a'): ['A -> B b', 'A -> a'],
                    ('A', 'b'): ['A -> B b'],
                    ('A', 'c'): ['A -> B b'],
                    ('A', 'd'): ['A -> B b'],
                    ('A', 'g'): ['A -> B b', 'A -> _'],
                    ('A', 'h'): ['A -> B b'],
                    ('A', '$'): ['A -> _'],

                    ('B', 'a'): ['B -> C c'],
                    ('B', 'b'): ['B -> C c', 'B -> b', 'B -> _'],
                    ('B', 'c'): ['B -> C c'],
                    ('B', 'd'): ['B -> C c'],
                    ('B', 'g'): ['B -> C c'],
                    ('B', 'h'): ['B -> C c'],

                    ('C', 'a'): ['C -> D d'],
                    ('C', 'b'): ['C -> D d'],
                    ('C', 'c'): ['C -> D d', 'C -> c', 'C -> _'],
                    ('C', 'd'): ['C -> D d'],
                    ('C', 'g'): ['C -> D d'],
                    ('C', 'h'): ['C -> D d'],

                    ('D', 'a'): ['D -> A g'],
                    ('D', 'b'): ['D -> A g'],
                    ('D', 'c'): ['D -> A g'],
                    ('D', 'd'): ['D -> A g', 'D -> _'],
                    ('D', 'g'): ['D -> A g'],
                    ('D', 'h'): ['D -> A g', 'D -> h'],
                },
                True,
            ),
            (       # i = 12
                (
                    'A -> B b | a | _',
                    'B -> C c | b | _',
                    'C -> D d',
                    'D -> A g | h | _',
                ),
                (
                    'A -> B b | a | _',
                    'B -> C c | b | _',
                    'C -> D d',
                    'D -> A g | h | _',
                ),
                (
                    'A -> B b | a | _',
                    'B -> C c | b | _',
                    'C -> D d',
                    'D -> A g | h | _',
                ),
                {
                    'A': [{'a', 'b', 'd', 'g', 'h', '_'}, 0],
                    'B': [{'a', 'b', 'd', 'g', 'h', '_'}, 0],
                    'C': [{'a', 'b', 'd', 'g', 'h'}, 1],
                    'D': [{'a', 'b', 'd', 'g', 'h', '_'}, 0],
                },
                {
                    'A': {'$', 'g'},
                    'B': {'b'},
                    'C': {'c'},
                    'D': {'d'},
                },
                {
                    ('A', 'a'): ['A -> B b', 'A -> a'],
                    ('A', 'b'): ['A -> B b'],
                    ('A', 'd'): ['A -> B b'],
                    ('A', 'g'): ['A -> B b', 'A -> _'],
                    ('A', 'h'): ['A -> B b'],
                    ('A', '$'): ['A -> _'],

                    ('B', 'a'): ['B -> C c'],
                    ('B', 'b'): ['B -> C c', 'B -> b', 'B -> _'],
                    ('B', 'd'): ['B -> C c'],
                    ('B', 'g'): ['B -> C c'],
                    ('B', 'h'): ['B -> C c'],

                    ('C', 'a'): ['C -> D d'],
                    ('C', 'b'): ['C -> D d'],
                    ('C', 'd'): ['C -> D d'],
                    ('C', 'g'): ['C -> D d'],
                    ('C', 'h'): ['C -> D d'],

                    ('D', 'a'): ['D -> A g'],
                    ('D', 'b'): ['D -> A g'],
                    ('D', 'd'): ['D -> A g', 'D -> _'],
                    ('D', 'g'): ['D -> A g'],
                    ('D', 'h'): ['D -> A g', 'D -> h'],
                },
                True,
            ),
            (       # i = 13
                (
                    'S -> A S B | a',
                    'A -> a A | _',
                    'B -> b B | _',
                ),
                (
                    'S -> A S B | a',
                    'A -> a A | _',
                    'B -> b B | _',
                ),
                (
                    'S -> A S B | a',
                    'A -> a A | _',
                    'B -> b B | _',
                ),
                {
                    'S': [{'a', 'b', '_'}, 0],
                    'A': [{'a', '_'}, 0],
                    'B': [{'b', '_'}, 0],
                },
                {
                    'S': {'$', 'b'},
                    'A': {'$', 'a', 'b'},
                    'B': {'$', 'b'},
                },
                {
                    ('S', 'a'): ['S -> A S B', 'S -> a'],
                    ('S', 'b'): ['S -> _', 'S -> A S B'],
                    ('S', '$'): ['S -> _'],
                    ('A', 'a'): ['A -> a A', 'A -> _'],
                    ('A', 'b'): ['A -> _'],
                    ('A', '$'): ['A -> _'],
                    ('B', 'b'): ['B -> b B', 'B -> _'],
                    ('B', '$'): ['B -> _'],
                },
                True,
            ),
        )
        for i, (rules_1, rules_2, rules_3, first_symbols, next_symbols, m_table,
                is_ambiguous) in enumerate(data):
            with self.subTest(i=i):
                bnf_1 = BNF()
                for line in rules_1:
                    bnf_1.add_rule(check.parse_line(line))

                bnf_2 = common_factors.remove(bnf_1)
                self.assertEqual(len(bnf_2.rules), len(rules_2))
                for e in zip(bnf_2.rules.values(), rules_2):
                    self.assertEqual(str(e[0]), e[1])

                bnf_3 = recursion.remove(bnf_2)
                self.assertEqual(len(bnf_3.rules), len(rules_3))
                for e in zip(bnf_3.rules.values(), rules_3):
                    self.assertEqual(str(e[0]), e[1])

                for symbol, (final_set, final_error_list_len) in first_symbols.items():
                    with self.subTest(s=symbol):
                        fs_set, error_list, error_symbol = ambiguity.get_first_symbols(
                            symbol, [], bnf_3)
                        self.assertEqual(fs_set, final_set)
                        self.assertEqual(len(error_list), final_error_list_len)
                        self.assertIsNone(error_symbol)     # all errors should have been resolved

                ns_dict = ambiguity.get_next_symbols(bnf_3)
                for symbol, final_set in next_symbols.items():
                    with self.subTest(s=symbol):
                        self.assertEqual(ns_dict[symbol], final_set)

                if m_table:
                    m = ambiguity.get_m_table(bnf_3, ns_dict)
                    self.assertEqual(len(m), len(m_table))
                    for key, rule_list in m.items():
                        with self.subTest(key=key):
                            self.assertEqual([str(r) for r in rule_list], m_table.get(key))

                self.assertEqual(ambiguity.check(bnf_3), is_ambiguous)


if __name__ == '__main__':
    unittest.main()
